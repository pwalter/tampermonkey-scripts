// ==UserScript==
// @name         gitlab-redirect
// @namespace    https://www.ebi.ac.uk
// @version      0.2
// @description  Redirect from gitlabci.ebi.ac.uk to gitlab.ebi.ac.uk
// @author       Peter Walter (pwalter@ebi.ac.uk)
// @match        https://gitlabci.ebi.ac.uk/*
// @updateURL    https://gitlab.ebi.ac.uk/pwalter/tampermonkey-scripts/raw/master/gitlab-redirect.user.js
// @downloadURL  https://gitlab.ebi.ac.uk/pwalter/tampermonkey-scripts/raw/master/gitlab-redirect.user.js
// @supportURL   https://gitlab.ebi.ac.uk/pwalter/tampermonkey-scripts/issues
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    document.location = document.location.href.replace('//gitlabci.ebi.ac.uk', '//gitlab.ebi.ac.uk');
})();
